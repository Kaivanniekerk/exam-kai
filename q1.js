// Auther: Kai Van Niekerk - 9960174
// Title: Question 1 - Test A
// Date: 29/03/2018

var degcel              //variable for Degrees Celcius
var degfah              //variable for Degrees Fahrenheit
var userinput       //variable for User Input



degcel = Number(prompt("Please input a temperature in degrees Celcius to be converted")); // prompt for temp in celc
degfah = (degcel * 18 / 10 + 32); //to get deg in fahrenheit you X(times) celc deg by 18 then devide by 10 + 32 

console.log ("This application will convert degrees Celcius to degrees Fahrenheit") // what shows on screen
console.log ("-------------------------------------------------------------------") // ""
console.log (`${userinput}  degrees Celcius is equal to ${degfah} degrees Fahrenheit`) // result 
